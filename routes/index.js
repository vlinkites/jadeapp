var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  var data = require('../json/index.json');
  res.render('index', {data : data});
});
/* GET privacy page. */
router.get('/privacy', function(req, res) {
  var data = require('../json/privacy.json');
  res.render('privacy', {data : data});
});
/* GET removesoftware page. */
router.get('/removesoftware', function(req, res) {
  var data = require('../json/removesoftware.json');
  res.render('removesoftware', {data : data});
});
/* GET contact page. */
router.get('/contact', function(req, res) {
  var data = require('../json/contact.json');
  res.render('contact', {data : data});
});
/* GET installer page. */
router.get('/installer', function(req, res) {
  var data = require('../json/installer.json');
  res.render('installer', {data : data});
});
/* GET firefox page. */
router.get('/firefox', function(req, res) {
  var data = require('../json/firefox.json');
  res.render('firefox', {data : data});
});
/* GET openoffice page. */
router.get('/openoffice', function(req, res) {
  var data = require('../json/openoffice.json');
  res.render('openoffice', {data : data});
});
/* GET utorrent page. */
router.get('/utorrent', function(req, res) {
  var data = require('../json/utorrent.json');
  res.render('utorrent', {data : data});
});



module.exports = router;
